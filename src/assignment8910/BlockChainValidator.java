package assignment8910;
import java.util.List;

public class BlockChainValidator {

	private BlockChainValidator() {

	}

	/**
	 * @param blockChain
	 * @return
	 */
//	check the chain validation if it ok......like bank transaction
	public static boolean isChainValid(BlockChain blockChain) {

		boolean isValid = true;
		String previousHash = BlockChain.INITIAL_HASH;
		List<Block> chain = blockChain.getChain();
		for (int i = 1; i <= chain.size(); i++) {

			Block block = chain.get(i - 1);
			block.previousBlockHash = previousHash;
			
			if (!block.hash.startsWith(BlockChain.HASH_PATTERN)) {
				System.out.println("Invalid Block " + i);
				isValid = false;
			}
			
			if (!block.hash.equals(block.calculateHash())) {
				System.out.println("Invalid Block " + i);
				isValid = false;
			}

			if (!block.previousBlockHash.equals(previousHash)) {
				System.out.println("Invalid Block " + i);
				isValid = false;
			}
			previousHash = block.calculateHash();
		}
		if(isValid) {
			System.out.println("Chain is Valid");
		}else {
			System.out.println("Chain is not Valid");			
		}
		return isValid;
	}

	/**
	 * @param blockChain
	 * @param block
	 * @return
	 */
	public static boolean isBlockValid(BlockChain blockChain, Block block) {

		if (!block.hash.equals(block.calculateHash())) {
			System.out.println("Invalid Block");
			return false;
		}

		List<Block> chain = blockChain.getChain();
		int blockIndex = chain.indexOf(block);
		if (blockIndex == 0) {
			// first block
			if (!block.previousBlockHash.equals(BlockChain.INITIAL_HASH)) {
				System.out.println("Invalid Block " + (blockIndex + 1));
				return false;
			}

		} else {
			Block previousBlock = chain.get(blockIndex - 1);
			if (!block.previousBlockHash.equals(previousBlock.hash)) {
				System.out.println("Invalid Block " + (blockIndex + 1));
				return false;
			}
		}
		System.out.println("Block is Valid");
		return true;
	}

}


