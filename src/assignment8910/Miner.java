package assignment8910;

/**
 * Mine a NonsenseCoin
 *
 */
public class Miner {

//	minig of block till its start with pattern 
	public Block generateBlock(String data, String previousBlockHash) {

		Block block = new Block(data, previousBlockHash);

		// Search for a Number which generates the required Hash pattern
		while (!block.calculateHash().startsWith(BlockChain.HASH_PATTERN)) {
			// Generate a new Number and set it to Block
			block.setNumber(NumberGenerator.generate());
		}
		block.rehash();
		return block;
	}

	public static void main(String args[]) {

		Miner miner = new Miner();
		Block block1 = miner.generateBlock("Data1", BlockChain.INITIAL_HASH);

		Block block2 = miner.generateBlock("Data2", block1.hash);

		System.out.println(block1.hash);
		System.out.println(block2.hash);

	}

}


