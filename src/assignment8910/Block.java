package assignment8910;

public class Block {
//input of block
	String data;
//output of block
	String hash;

	String previousBlockHash;
	
	String number = NumberGenerator.generate();

	public void setNumber(String number) {
		this.number = number;
	}
	
	public void rehash() {
		this.hash = calculateHash();
	}

	public Block(String data, String previousBlockHash) {
		this.data = data;
		this.previousBlockHash = previousBlockHash;
		this.hash = calculateHash();
	}

	public void setData(String data) {
		this.data = data;
		// When data changes the hash also changes
		this.hash = calculateHash();
	}

	public String calculateHash() {
		return HashBuilder.buildHash(this.previousBlockHash + this.data + this.number);
	}

}


