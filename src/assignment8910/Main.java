package assignment8910;


import java.util.List;

public class Main {

	public static void main(String args[]) {

		// Build Block Chain
		buildBlockChain();

		// List all Hashes of Block chain
		listAllHashes();

		// Validating Block Chain
		validateBlockChain();

		// Way to show that when data changes into any block the subsequent blocks
		// become invalid
		blockDataChangeTest();

		// Validating Individual Blocks
		validateIndividualBlocks();

	}

	private static BlockChain buildBlockChain() {

		Miner miner = new Miner(); 
		BlockChain blockChain = new BlockChain();

		Block firstBlock = miner.generateBlock("Data1", BlockChain.INITIAL_HASH);
		blockChain.addBlock(firstBlock, null);
		// Output the Hash
		System.out.println("Fisrt Block Hash : " + firstBlock.hash);

		Block secondBlock = miner.generateBlock("Data2", firstBlock.hash);
		blockChain.addBlock(secondBlock, null);
		System.out.println("Second Block Hash : " + secondBlock.hash);

		Block thirdBlock = miner.generateBlock("Data3", secondBlock.hash);
		blockChain.addBlock(thirdBlock, null);
		System.out.println("Third Block Hash : " + thirdBlock.hash);

		Block fourthBlock = miner.generateBlock("Data4", thirdBlock.hash);
		blockChain.addBlock(fourthBlock, null);
		System.out.println("Fourth Block Hash : " + fourthBlock.hash);

		return blockChain;
	}

	private static void listAllHashes() {

		BlockChain blockChain = buildBlockChain();

		System.out.println("Listing all Hashes of the Block chain");
		for (Block block : blockChain.getChain()) {
			System.out.println(block.hash);
		}
	}

	private static void validateBlockChain() {

		BlockChain blockChain = buildBlockChain();
		System.out.println("Validating Block Chain");
		boolean isTransactionValid = BlockChainValidator.isChainValid(blockChain);
		if (isTransactionValid) {
			System.out.println("The transaction is valid");
		}
		System.out.println();
	}

	private static void blockDataChangeTest() {
//            to check the block data
		BlockChain blockChain = buildBlockChain();
		List<Block> chain = blockChain.getChain();
		Block firstBlock = chain.get(0);
		Block secondBlock = chain.get(1);
		Block thirdBlock = chain.get(2);
		Block fourthBlock = chain.get(3);

		System.out.println("Changing data of Block 3");
		thirdBlock.setData("dummy");
		System.out.println();

		System.out.println("Validating Block Chain");
		BlockChainValidator.isChainValid(blockChain);
		System.out.println();

		System.out.println("Putting back same data in Block 3");
		thirdBlock.setData("Data3");
		System.out.println();

		System.out.println("Validating Block Chain");
		BlockChainValidator.isChainValid(blockChain);
		System.out.println();

		System.out.println("Changing data of Block 1");
		firstBlock.setData("dummy");
		System.out.println();

		System.out.println("Validating Block Chain");
		BlockChainValidator.isChainValid(blockChain);
		System.out.println();

	}

	private static void validateIndividualBlocks() {
		BlockChain blockChain = buildBlockChain();
		for (Block block : blockChain.getChain()) {
			BlockChainValidator.isBlockValid(blockChain, block);
		}
	}

}

