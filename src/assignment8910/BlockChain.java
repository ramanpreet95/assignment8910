package assignment8910;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class BlockChain {

	// List of Blocks
	List<Block> chain = new LinkedList<>();

	public BlockChain() {
		chain = new LinkedList<>();
	}

	public List<Block> getChain() {
		return chain;
	}

	// Map to keep track of Non sense coins each user has
	Map<String, Integer> coinDistributionMap = new HashMap<>();

	// The pattern the each Hash should follow, 4 leading zeros
	public static final String HASH_PATTERN = "0000";

	// Initial previous Hash value for first block
	public static final String INITIAL_HASH = "00001";
 
//	block will consider valid if if its equal to haspattern
	public boolean addBlock(Block block, String userId) {

		// Validate Hash Pattern
		if (!block.hash.startsWith(HASH_PATTERN)) {
			System.out.println(
					"Invalid Block Hash. The block Hash should start with " + HASH_PATTERN + ". Block not added.");
			return false;
		}

		// Validate if the Hash is generated using a valid Number
//		without calculation
		if (!block.hash.equals(HashBuilder.buildHash(block.previousBlockHash + block.data + block.number))) {
			System.out.println("Block Hash is not valid. Block not added.");
			return false;
		}

		// Compare the block's previousHashBlock value with the Hash of previous Block
		if (chain.size() > 1) {
			Block previousBlock = chain.get(chain.size() - 1);
			if (block.previousBlockHash.equals(previousBlock.hash)) {
				chain.add(block);
				sendRewardToUser(userId);
				System.out.println("Block added to chain.");
				return true;
			} else {
				System.out.println("Invalid Block. Block not added.");
				return false;
			}
		}
		chain.add(block);
		System.out.println("Block added to chain.");
		return true;
	}

	/**
	 * Maintain a record of how many NonseseCoins each user has
	 * 
	 */
	private void sendRewardToUser(String userId) {

		if (coinDistributionMap.containsKey(userId)) {
			int noOfCoinsTheUserHas = coinDistributionMap.get(userId);
			noOfCoinsTheUserHas = noOfCoinsTheUserHas + 1;
			coinDistributionMap.put(userId, noOfCoinsTheUserHas);
		} else {
			coinDistributionMap.put(userId, 1);
		}
		System.out.println("The User " + userId + " GET A NONSENSE COIN.");

	}

}

